package ru.tsc.pavlov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity {

   @NotNull
   public String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

}
