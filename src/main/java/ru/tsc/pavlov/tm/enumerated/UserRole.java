package ru.tsc.pavlov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum UserRole {
    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private String displayName;

    UserRole(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
