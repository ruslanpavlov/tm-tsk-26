package ru.tsc.pavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.IOwnerRepository;
import ru.tsc.pavlov.tm.api.service.IOwnerService;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService <E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    @NotNull private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public void clear(@Nullable final String userId) {
        repository.clear(userId);
    }

    @NotNull
    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    public E removeById(@NotNull String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
