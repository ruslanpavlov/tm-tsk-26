package ru.tsc.pavlov.tm.api.entity;

import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable Status getStatus();

    void setStatus(@Nullable Status status);

}
