package ru.tsc.pavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.UserNotFoundException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.USER_UPDATE_BY_LOGIN;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User update by login";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER USER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER USER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER USER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER USER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();

        getUserService().updateByLogin(login, lastName, firstName, middleName, email);

        System.out.println("USER UPDATED");
    }

}
