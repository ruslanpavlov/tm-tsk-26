package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.api.service.IAuthService;
import ru.tsc.pavlov.tm.api.service.IProjectTaskService;
import ru.tsc.pavlov.tm.command.AbstractCommand;

public abstract class AbstractMutuallyCommand extends AbstractCommand {

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    protected IAuthService getAuthService() { return serviceLocator.getAuthService(); }

}
