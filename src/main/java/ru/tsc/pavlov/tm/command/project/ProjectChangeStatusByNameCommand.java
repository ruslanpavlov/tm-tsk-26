package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by name";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @Nullable final Project project = getProjectService().changeStatusByName(userId, name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

}
